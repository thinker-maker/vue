import Autocomplete from 'vuejs-auto-complete'

export default {
    data() {
        return {
            success: false,
            books: this.$store.getters.getBooks,
            newBook: {
                id: '',
                isbn: '',
                title: '',
                author: '',
                description: '',
                created: ''
            },
        }
    },
    components: {
        Autocomplete
    },
    methods: {
        addBook: function() {
            var currentDate = new Date();
            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            this.newBook.created = month + "-" + day + "-" + year;
            this.newBook.id = this.$store.getters.getBooks.length;
            this.$store.dispatch('create', this.newBook);
            this.$emit('addSuccess', true);
            this.$router.push({name: 'books', params: {addSuccess: true}});
        },
        updateBook: function() {
            this.$store.dispatch('update', this.newBook);
            this.$emit('updateSuccess', true);
            this.$router.push({name: 'books', params: {updateSuccess: true}});
        },
        addDistributionGroup (group) {
            this.newBook.author = group.display;
        },
    },
    created() {
        let self = this;
        if (self.$route.query.id) {
            self.$store.getters.getBooks.forEach(function(val) {
                if(val.id == self.$route.query.id) {
                    self.newBook = val;
                }
            });
        }
    }
}