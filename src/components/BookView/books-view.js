export default {
    data() {
        return {
            bookInArray: -1
        }
    },
    computed: {
        findBook() {
            var id = this.$route.params.id;
            let books = this.$store.getters.getBooks;
            let result =  books.filter(
                function(o) {
                    return o.id == id;
                }
            );
            this.bookInArray = this.$store.getters.getBooks.indexOf(result[0]);
            return result[0];
        }
    },
    methods: {
        removeBook() {
            if(this.bookInArray != -1) {
                this.$store.dispatch('remove', this.bookInArray);
                this.$emit('removeSuccess', true);
                this.$router.push({name: 'books', params: {removeSuccess: true}});
            }
        }
    }
}