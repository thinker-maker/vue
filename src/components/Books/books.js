import Pagination from './../Pagination.vue'

export default {
    data() {
        return {
            books: [],
            allBooks: this.$store.getters.getBooks,
            searchBooks: [],
            perPage: 3,
            currentPage: 1,
            searchField: '',
            notFound: false,
            showAddMessage: this.addSuccess,
            showRemoveMessage: this.removeSuccess,
            showUpdateMessage: this.updateSuccess
        }
    },
    computed: {
        totalPages() {
            var pagesArr = [];
            for (let i = 0; i < Math.ceil(this.allBooks.length / this.perPage); i++) {
                pagesArr.push(i + 1);
            }
            return pagesArr;
        },
        searchList(val){
            this.allBooks.forEach(function(val) {
                if(val.title.toLowerCase().includes(this.searchList.toLowerCase())){
                    this.books.push(val);
                }
            })
        }
    },
    props: [
        'addSuccess',
        'updateSuccess',
        'removeSuccess'
    ],
    methods: {
        getPage($event) {
            this.setPage($event.target.value);
        },
        setPage(pageNumber) {
            this.books = [];
            var currentAllBooks = this.allBooks;
            if(this.searchField != "" && this.searchBooks.length > 0) {
                currentAllBooks = this.searchBooks;
                this.notFound = false;
            } else if(this.searchField != "" && this.searchBooks.length == 0) {
                this.notFound = true;
                return;
            }
            this.currentPage = pageNumber;
            for (var i = (this.currentPage - 1) * this.perPage; i < (this.currentPage * this.perPage); i++){
                if(typeof currentAllBooks[i] !== 'undefined'){
                    this.books.push(currentAllBooks[i]);
                }
            }
        },
        filterRecords() {
            this.searchBooks = [];
            this.allBooks = this.$store.getters.getBooks;
            let self = this;
            this.allBooks.forEach(function(val) {
                if(val.title.toLowerCase().includes(self.searchField.toLowerCase())){
                    if(typeof val !== 'undefined') {
                        self.searchBooks.push(val);
                    }
                }
            });
            this.allBooks = this.searchBooks;
            this.setPage(1);
        },
        updateMessages() {
            var self = this;
            if(self.showAddMessage || self.showRemoveMessage || self.showUpdateMessage) {
                setTimeout(function() {
                    self.showAddMessage = false;
                    self.showRemoveMessage = false;
                    self.showUpdateMessage = false;
                }, 2000);
            }
        }
    },
    components:{
        Pagination
    },
    beforeMount(){
        this.setPage(1)
    },
    mounted() {
        this.updateMessages();
    }
}