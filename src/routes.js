import Books from './components/Books/Books.vue';
import Home from './components/Home.vue';
import BookAdd from './components/BookAdd/BookAdd.vue';
import BookView from './components/BookView/BookView.vue';

export const routes = [
    { path: '', component: Home },
    { path: '/books', component: Books, name: 'books', props: true},
    { path: '/book/:id', component: BookView, name: 'view' },
    { path: '/add', component: BookAdd, name: 'add'}
];