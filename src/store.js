import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        books: [
            {
                id: '1',
                isbn: '978-0140177381',
                title: 'Cannery Row',
                author: 'John Steinbeck',
                description: 'Drawing on his memories of the real inhabitants of Monterey, California, including longtime friend Ed Ricketts, Steinbeck interweaves the stories of Doc, Dora, Mack and his boys, Lee Chong, and the other characters in this world where only the fittest survive, to create a novel that is at once one of his most humorous and poignant works.',
                created: '2-18-2018'
            },
            {
                id: '2',
                isbn: '978-1781396834',
                title: 'The Great Gatsby',
                author: 'F. Scott Fitzgerald',
                description: 'Only in the subsequent years, after Fitzgerald’s death, has the novel The Great Gatsby come be seen as perhaps the great 20th century American novel, and the character Jay Gatsby, as one of the truly mythological creations in American culture.',
                created: '3-1-2018'
            },
            {
                id: '3',
                isbn: '978-1781396854',
                title: 'The Road',
                author: 'Cormac McCarthy',
                description: 'A father and his son walk alone through burned America. Nothing moves in the ravaged landscape save the ash on the wind. It is cold enough to crack stones, and when the snow falls it is gray. The sky is dark. Their destination is the coast, although they don\'t know what, if anything, awaits them there. They have nothing; just a pistol to defend themselves against the lawless bands that stalk the road, the clothes they are wearing, a cart of scavenged food—and each other.',
                created: '2-25-2018'
            },
            {
                id: '4',
                isbn: '978-1501142970',
                title: 'It',
                author: 'Stephen King',
                description: 'Welcome to Derry, Maine. It’s a small city, a place as hauntingly familiar as your own hometown. Only in Derry the haunting is real.',
                created: '12-20-2017'
            },
            {
                id: '5',
                isbn: '978-1503275188',
                title: 'Great Expectations',
                author: 'Charles Dickens',
                description: 'Great Expectations is Charles Dickens\'s thirteenth novel. It is his second novel, after David Copperfield, to be fully narrated in the first person. Great Expectations is a bildungsroman, or a coming-of-age novel, and it is a classic work of Victorian literature.',
                created: '3-5-2018'
            },
        ]
    },
    getters: {
        getBooks: state => {
            return state.books;
        }
    },
    mutations: {
        createBook: (state, payload) => {
            state.books.push(payload);
        },
        updateBook: (state, payload) => {
            let objIndex = state.books.findIndex(obj => obj.id == payload.id);
            state.books.splice(objIndex, 1, payload);
        },
        deleteBook: (state, payload) => {
            state.books.splice(payload, 1);
        }
    },
    actions: {
        create({ commit }, payload) {
            commit('createBook', payload);
        },
        update({ commit }, payload) {
            commit('updateBook', payload);
        },
        remove({ commit }, payload) {
            commit('deleteBook', payload);
        }
    }
});
